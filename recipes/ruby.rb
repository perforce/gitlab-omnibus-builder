# Install Ruby for use by omnibus and gitlab-runner
#
# Depending on the platform, we get Ruby 1.9.3 or newer, installed in either
# /usr/bin or /usr/local/bin.

case node['platform_family']
when 'debian'
  # Ruby 1.9.3
  package 'ruby1.9.1'
  package 'ruby1.9.1-dev'
when 'rhel'
  case node['platform_version']
  when /^7\./
    # This gives us Ruby 2.0
    package 'ruby'
    package 'ruby-devel'
  when /^6\./
    # On RHEL 6, install Ruby 2.1.5 from source
    %w{
    gcc openssl-devel libyaml-devel libffi-devel readline-devel zlib-devel
    gdbm-devel ncurses-devel
    }.each do |pkg|
      package pkg
    end

    ruby_build_install_dir = '/var/cache/ruby-build'

    git ruby_build_install_dir do
      repository 'https://github.com/sstephenson/ruby-build.git'
      notifies :run, 'execute[install ruby-build]', :immediately
    end

    execute 'install ruby-build' do
      command './install.sh'
      cwd ruby_build_install_dir
      action :nothing
    end

    ruby_version = node['gitlab-omnibus-builder']['ruby_version']
    execute "/usr/local/bin/ruby-build #{ruby_version} /usr/local" do
      not_if "/usr/local/bin/ruby --version | grep 'ruby #{ruby_version}'"
    end
  end
end

execute 'gem install bundler --no-ri --no-rdoc' do
  # Make sure we do not pick up the 'gem' executable from /opt/chef.
  environment 'PATH' => '/usr/local/bin:/usr/bin:/bin'
  creates '/usr/local/bin/bundle'
end
