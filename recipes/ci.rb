# Recipe: ci
#
# This recipe sets up a CI-controlled omnibus-gitlab build environment. If you
# do not want or need GitLab CI, use the 'default' recipe instead.

include_recipe 'gitlab-omnibus-builder::default'

# Set up gitlab-runner, so we can trigger builds from GitLab CI
include_recipe 'gitlab-omnibus-builder::gitlab-runner'

# Install Runit to supervise gitlab-runner
include_recipe 'gitlab-omnibus-builder::runit'

# Create a Runit service that supervises gitlab-runner
include_recipe 'gitlab-omnibus-builder::runit_service'

# Install secrets for fetching EE source code, pushing packages
include_recipe 'gitlab-omnibus-builder::secrets'
