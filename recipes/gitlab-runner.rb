# Install gitlab-runner so that we can trigger omnibus builds from GitLab CI.

case node['platform_family']
when 'debian'
  package 'libicu-dev'
when 'rhel'
  package 'libicu-devel'
end

username = node['gitlab-omnibus-builder']['username']
runner_dir = node['gitlab-omnibus-builder']['runner_dir']

git runner_dir do
  user username
  repository 'https://gitlab.com/gitlab-org/gitlab-ci-runner.git'
  revision 'master'
  action :sync
  notifies :run, 'execute[bundle install]'
end

execute 'bundle install' do
  command "bundle install --path #{File.join(node['gitlab-omnibus-builder']['home'], 'gems')}"
  # Make sure we do not pick up the 'bundle' executable from /opt/chef.
  environment 'PATH' => '/usr/local/bin:/usr/bin:/bin'
  user username
  cwd runner_dir
  action :nothing
end
